
# step 1: imagem base aspnet 6
FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 8090
EXPOSE 443

# step 2: imagem de buid sdk .net6
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Source/ConsultaFIPE.Apresentacao.WebApi/ConsultaFIPE.Apresentacao.WebApi.csproj", "Source/ConsultaFIPE.Apresentacao.WebApi/"]
COPY ["Source/ConsultaFIPE.Dominio.ServicosExternos/ConsultaFIPE.Dominio.ServicosExternos.csproj", "Source/ConsultaFIPE.Dominio.ServicosExternos/"]
COPY ["Source/ConsultaFIPE.Dominio.Entidade/ConsultaFIPE.Dominio.Entidade.csproj", "Source/ConsultaFIPE.Dominio.Entidade/"]
COPY ["Source/ConsultaFIPE.Dominio.Negocio/ConsultaFIPE.Dominio.Negocio.csproj", "Source/ConsultaFIPE.Dominio.Negocio/"]
COPY ["Source/ConsultaFIPE.Infraestrutura.ServicosExternos/ConsultaFIPE.Infraestrutura.ServicosExternos.csproj", "Source/ConsultaFIPE.Infraestrutura.ServicosExternos/"]
COPY ["Source/ConsultaFIPE.Infraestrutura.Utilidades/ConsultaFIPE.Infraestrutura.Utilidades.csproj", "Source/ConsultaFIPE.Infraestrutura.Utilidades/"]
COPY ["Source/ConsultaFIPE.Infraestrutura.RepositorioEF/ConsultaFIPE.Infraestrutura.RepositorioEF.csproj", "Source/ConsultaFIPE.Infraestrutura.RepositorioEF/"]
RUN dotnet restore "Source/ConsultaFIPE.Apresentacao.WebApi/ConsultaFIPE.Apresentacao.WebApi.csproj"
COPY . .
WORKDIR "/src/Source/ConsultaFIPE.Apresentacao.WebApi"
RUN dotnet build "ConsultaFIPE.Apresentacao.WebApi.csproj" -c Release -o /app/build

# step 4: imagem publisher
FROM build AS publish
RUN dotnet publish "ConsultaFIPE.Apresentacao.WebApi.csproj" -c Release -o /app/publish /p:UseAppHost=false

# step 5: imagem final de deploy
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ConsultaFIPE.Apresentacao.WebApi.dll"]