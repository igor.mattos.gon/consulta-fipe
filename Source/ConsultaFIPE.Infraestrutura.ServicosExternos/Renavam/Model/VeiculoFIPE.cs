﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.Model
{
    [DataContract]
    public class DadosVeiculoFIPE
    {
        [JsonProperty(PropertyName = "data")]
        public DataVeiculoFIPE Data { get; set; }
    }

    [DataContract]
    public class DataVeiculoFIPE
    {
        [JsonProperty(PropertyName = "veiculo")]
        public VeiculoFIPE Veiculo { get; set; }
    }

    [DataContract]
    public class VeiculoFIPE
    {
        [JsonProperty(PropertyName = "uf")]
        public string UF { get; set; }

        [JsonProperty(PropertyName = "ano")]
        public string Ano { get; set; }

        [JsonProperty(PropertyName = "cmt")]
        public string CMT { get; set; }

        [JsonProperty(PropertyName = "cor")]
        public string Cor { get; set; }

        [JsonProperty(PropertyName = "pbt")]
        public string PBT { get; set; }

        [JsonProperty(PropertyName = "placa")]
        public string Placa { get; set; }

        [JsonProperty(PropertyName = "chassi")]
        public string Chassi { get; set; }

        [JsonProperty(PropertyName = "n_motor")]
        public string Motor { get; set; }

        [JsonProperty(PropertyName = "potencia")]
        public string Potencia { get; set; }

        [JsonProperty(PropertyName = "municipio")]
        public string Municipio { get; set; }        

        [JsonProperty(PropertyName = "cilindrada")]
        public string Cilindrada { get; set; }

        [JsonProperty(PropertyName = "combustivel")]
        public string Combustivel { get; set; }

        [JsonProperty(PropertyName = "procedencia")]
        public string Procedencia { get; set; }

        [JsonProperty(PropertyName = "marca_modelo")]
        public string MarcaModelo { get; set; }

        [JsonProperty(PropertyName = "tipo_montagem")]
        public string Montagem { get; set; }

        [JsonProperty(PropertyName = "tipo_de_veiculo")]
        public string Tipo { get; set; }

        [JsonProperty(PropertyName = "capacidade_de_carga")]
        public string CapacidadeCarga { get; set; }

        [JsonProperty(PropertyName = "quantidade_de_eixos")]
        public int? QtdEixo { get; set; }

        [JsonProperty(PropertyName = "quantidade_passageiro")]
        public int? QtdPassageiro { get; set; }
    }
}
