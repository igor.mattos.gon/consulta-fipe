﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.Model
{
    public class FIPEParameters
    {
        public string URL { get; set; }

        public string Chave { get; set; }
    }
}
