﻿using ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.FIPE
{
    public interface IIntegrationRenavamFIPE
    {
        /// <summary>
        /// Método que lista o veículo por placa
        /// </summary>
        /// <param name="placa">Placa do Veículo</param>
        /// <returns>Veículo</returns>
        /// <exception cref="Exception">Erro de consulta</exception>
        Task<VeiculoFIPE> ListarVeiculoPorPlaca(string placa);
    }
}
