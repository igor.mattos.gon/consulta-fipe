﻿using ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.Model;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.FIPE
{
    public class IntegrationRenavamFIPE : IIntegrationRenavamFIPE
    {
        private readonly HttpClient httpClient;
        private HttpResponseMessage httpResponseMessage;
        private readonly FIPEParameters _FIPEParameters;

        public IntegrationRenavamFIPE(IOptions<FIPEParameters> FIPEParameters)
        {
            _FIPEParameters = FIPEParameters.Value;

            httpClient = new HttpClient();
            httpClient.Timeout = TimeSpan.FromSeconds(30);
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Método que lista o veículo por placa
        /// </summary>
        /// <param name="placa">Placa do Veículo</param>
        /// <returns>Veículo</returns>
        /// <exception cref="Exception">Erro de consulta</exception>
        public async Task<VeiculoFIPE> ListarVeiculoPorPlaca(string placa)
        {
            httpClient.BaseAddress = new Uri(_FIPEParameters.URL);                                   
            httpResponseMessage = await httpClient.GetAsync(placa + "?key=" + _FIPEParameters.Chave);

            //Preencha os dados do veículo
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                string jsonRetorno = await httpResponseMessage.Content.ReadAsStringAsync();

                DadosVeiculoFIPE dadosVeiculoFIPE = JsonConvert.DeserializeObject<DadosVeiculoFIPE>(jsonRetorno);
                return dadosVeiculoFIPE.Data.Veiculo;
            }

            else
            {
                string erroFIPE = await httpResponseMessage.Content.ReadAsStringAsync();                
                throw new Exception(erroFIPE);
            }            
        }
    }
}
