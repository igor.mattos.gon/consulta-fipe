﻿using ConsultaFIPE.Dominio.Entidade.Model;
using Sigtrans.Uranus.Net.UranusCore.Dominio.Usuario.Negocio.Contract.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Negocio.Contract.Services
{
    public interface IDomainClienteCicloUso : IDomainBase<ClienteCicloUso>
    {
        /// <summary>
        /// Método que cancela um ciclo de uso ativo
        /// </summary>
        /// <param name="idCicloUso">Id do Ciclo de Uso</param>
        /// <returns></returns>
        Task CancelarCicloUso(int idCicloUso, string login);
    }
}
