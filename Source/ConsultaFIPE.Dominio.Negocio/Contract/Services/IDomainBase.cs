﻿/// <summary>
/// @Author: Igor Gonçalves 2021
/// ----------------------------------
/// Copyright (c) 2020, SIGTRANS (http://sigtrans.com.br) Todos os direitos reservados.
/// 
/// SIGTRANS, Licença de software proprietário.
/// Você não pode utilizar esse software sem autorização prévia.
/// É estritamente proibida qualquer tipo de cópia, redistribuição, modificações e outras alterações. 
/// Para adquirir esse software utilize os canais oficiais ou por representantes certificados.
/// 
/// </summary>
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sigtrans.Uranus.Net.UranusCore.Dominio.Usuario.Negocio.Contract.Services
{
    public interface IDomainBase<TEntity> where TEntity : class
    {
        /// <summary>
        /// Busca a partir de um ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TEntity> ObterPorId(int id);

        /// <summary>
        /// Busca a partir de um ID.
        /// </summary>
        /// <param name="cod"></param>
        /// <returns></returns>
        TEntity ObterPorCodigo(string cod);

        /// <summary>
        /// Listar objetos
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> ListarTodos();

        /// <summary>
        /// Excluir um objeto
        /// </summary>
        /// <param name="item"></param>
        void Excluir(TEntity entidade);

        /// <summary>
        /// Excluir um objeto
        /// </summary>
        /// <param name="item"></param>
        void Excluir(int entidadeId);

        /// <summary>
        /// Inserir um objeto
        /// </summary>
        /// <param name="item"></param>
        Task Cadastrar(TEntity entidade);

        /// <summary>
        /// Alterar um objeto
        /// </summary>
        /// <param name="item"></param>
        Task Atualizar(TEntity entidade);

        /// <summary>
        /// Lista os objetos na base baseado em uma expressão exemplo: c => c.ativo == true
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> ListarporExpressao(Expression<Func<TEntity, bool>> predicate);
    }
}
