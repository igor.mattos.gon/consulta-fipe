﻿using ConsultaFIPE.Dominio.Entidade.Model;
using Sigtrans.Uranus.Net.UranusCore.Dominio.Usuario.Negocio.Contract.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Negocio.Contract.Services
{
    public interface IDomainCliente : IDomainBase<Cliente>
    {
    }
}
