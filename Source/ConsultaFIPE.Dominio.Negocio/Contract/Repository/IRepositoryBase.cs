﻿/// <summary>
/// @Author: Igor Gonçalves 2021
/// ----------------------------------
/// Copyright (c) 2020, SIGTRANS (http://sigtrans.com.br) Todos os direitos reservados.
/// 
/// SIGTRANS, Licença de software proprietário.
/// Você não pode utilizar esse software sem autorização prévia.
/// É estritamente proibida qualquer tipo de cópia, redistribuição, modificações e outras alterações. 
/// Para adquirir esse software utilize os canais oficiais ou por representantes certificados.
/// 
/// </summary>
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sigtrans.Uranus.Net.UranusCore.Dominio.Usuario.Negocio.Contract.Repository
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        /// <summary>
        /// Busca a partir de um ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TEntity> FindById(int id);

        /// <summary>
        /// Busca a partir de um ID.
        /// </summary>
        /// <param name="cod"></param>
        /// <returns></returns>
        TEntity FindByCode(string cod);

        /// <summary>
        /// Listar objetos
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> FindAll();

        /// <summary>
        /// Excluir um objeto
        /// </summary>
        /// <param name="item"></param>
        void Delete(TEntity entidade);

        /// <summary>
        /// Excluir um objeto
        /// </summary>
        /// <param name="item"></param>
        void Delete(int entidadeId);

        /// <summary>
        /// Inserir um objeto
        /// </summary>
        /// <param name="item"></param>
        Task Insert(TEntity entidade);

        /// <summary>
        /// Alterar um objeto
        /// </summary>
        /// <param name="item"></param>
        Task Update(TEntity entidade);

        /// <summary>
        /// Lista os objetos na base baseado em uma expressão exemplo: c => c.ativo == true
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> FindAllbyExpression(Expression<Func<TEntity, bool>> predicate);
    }
}
