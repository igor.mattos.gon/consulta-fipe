﻿using ConsultaFIPE.Dominio.Entidade.Model;
using Sigtrans.Uranus.Net.UranusCore.Dominio.Usuario.Negocio.Contract.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Negocio.Contract.Repository
{
    public interface IRepositoryHistoricoAtualizacaoCliente : IRepositoryBase<HistoricoAtualizacaoCliente>
    {
    }
}
