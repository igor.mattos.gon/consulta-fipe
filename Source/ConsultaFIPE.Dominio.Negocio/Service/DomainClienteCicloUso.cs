﻿using ConsultaFIPE.Dominio.Entidade.Enum;
using ConsultaFIPE.Dominio.Entidade.Model;
using ConsultaFIPE.Dominio.Negocio.Contract.Repository;
using ConsultaFIPE.Dominio.Negocio.Contract.Services;
using Microsoft.EntityFrameworkCore;
using Sigtrans.Uranus.Net.UranusCore.Dominio.Usuario.Negocio.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Negocio.Service
{
    public class DomainClienteCicloUso : DomainBase<ClienteCicloUso>, IDomainClienteCicloUso
    {
        private readonly IRepositoryClienteCicloUso _repositoryClienteCicloUso;
        private readonly IRepositoryCliente _repositoryCliente;

        public DomainClienteCicloUso(IRepositoryClienteCicloUso repositoryClienteCicloUso, IRepositoryCliente repositoryCliente)
            : base(repositoryClienteCicloUso)
        {
            _repositoryClienteCicloUso = repositoryClienteCicloUso;
            _repositoryCliente = repositoryCliente;
        }

        /// <summary>
        /// Método que cadastra um ciclo de uso da aplicação para o Cliente
        /// </summary>
        /// <param name="clienteCicloUso">Ciclo de Uso</param>
        /// <returns></returns>
        public override async Task Cadastrar(ClienteCicloUso clienteCicloUso)
        {
            try
            {
                #region Validação

                if(clienteCicloUso.DataInicio <= DateTime.Now)
                {
                    throw new Exception("A Data de Início não pode ser menor que a Data Atual.");
                }

                Cliente clienteCadastrado = await _repositoryCliente.FindAllbyExpression(f => f.UUID == clienteCicloUso.Cliente.UUID).FirstOrDefaultAsync();

                if (clienteCadastrado == null)
                {
                    throw new Exception("Cliente não cadastrado.");
                }

                if (clienteCadastrado != null && !clienteCadastrado.Ativo)
                {
                    throw new Exception(String.Format("Cliente '{0}' inativo.", clienteCadastrado.Nome));
                }

                ClienteCicloUso cicloUsoAtivo = await _repositoryClienteCicloUso.FindAllbyExpression(f => f.IdCliente == clienteCadastrado.IdCliente &&
                                                                                                          f.Status == (int)StatusEnum.ClienteCicloUso.Ativo)
                                                                                .FirstOrDefaultAsync();

                if (cicloUsoAtivo != null)
                {
                    throw new Exception(String.Format("Cliente '{0}' com Plano de Consulta Ativo. Cancele o mesmo ou aguarde a expiração antes de criar um novo."));
                }

                #endregion

                clienteCicloUso.IdCliente = clienteCadastrado.IdCliente;
                clienteCicloUso.Status = (int)StatusEnum.ClienteCicloUso.Ativo;
                clienteCicloUso.DataFim = clienteCicloUso.DataInicio.AddDays(clienteCicloUso.TempoVigencia);
                clienteCicloUso.Cliente = null;

                await base.Cadastrar(clienteCicloUso);
            }

            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Método que cancela um ciclo de uso ativo
        /// </summary>
        /// <param name="idCicloUso">Id do Ciclo de Uso</param>
        /// <returns></returns>
        public async Task CancelarCicloUso(int idCicloUso, string login)
        {
            try
            {
                ClienteCicloUso cicloUsoAtivo = await _repositoryClienteCicloUso.FindById(idCicloUso);

                #region Validação

                if (cicloUsoAtivo == null)
                {
                    throw new Exception("Plano de Consulta de Veículo não encontrado.");
                }

                if(cicloUsoAtivo != null && cicloUsoAtivo.Status != (int)StatusEnum.ClienteCicloUso.Ativo)
                {
                    throw new Exception("Somente Planos de Consulta Ativos podem ser cancelados.");
                }

                #endregion

                cicloUsoAtivo.Status = (int)StatusEnum.ClienteCicloUso.Cancelado;
                cicloUsoAtivo.Observacao = String.Format("Cancelado por '{0}' em '{1}'.", login, DateTime.Now.ToString(CultureInfo.GetCultureInfo("pt-BR")));

                await _repositoryClienteCicloUso.Update(cicloUsoAtivo);
            }

            catch
            {
                throw;
            }
        }
    }
}
