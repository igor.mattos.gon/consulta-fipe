﻿using ConsultaFIPE.Dominio.Entidade.Model;
using ConsultaFIPE.Dominio.Negocio.Contract.Repository;
using ConsultaFIPE.Dominio.Negocio.Contract.Services;
using Sigtrans.Uranus.Net.UranusCore.Dominio.Usuario.Negocio.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Negocio.Service
{
    public class DomainHistoricoAtualizacaoCliente : DomainBase<HistoricoAtualizacaoCliente>, IDomainHistoricoAtualizacaoCliente
    {
        private readonly IRepositoryHistoricoAtualizacaoCliente _repositoryHistoricoAtualizacaoCliente;

        public DomainHistoricoAtualizacaoCliente(IRepositoryHistoricoAtualizacaoCliente repositoryHistoricoAtualizacaoCliente)
            :base(repositoryHistoricoAtualizacaoCliente)
        {
            _repositoryHistoricoAtualizacaoCliente = repositoryHistoricoAtualizacaoCliente;
        }
    }
}
