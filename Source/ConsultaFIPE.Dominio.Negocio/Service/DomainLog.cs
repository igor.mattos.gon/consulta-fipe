﻿using ConsultaFIPE.Dominio.Entidade.Model;
using ConsultaFIPE.Dominio.Negocio.Contract.Repository;
using ConsultaFIPE.Dominio.Negocio.Contract.Services;
using Sigtrans.Uranus.Net.UranusCore.Dominio.Usuario.Negocio.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Negocio.Service
{
    public class DomainLog : DomainBase<Log>, IDomainLog
    {
        private readonly IRepositoryLog _repositoryLog;

        public DomainLog(IRepositoryLog repositoryLog)
            :base(repositoryLog)
        {
            _repositoryLog = repositoryLog;
        }
    }
}
