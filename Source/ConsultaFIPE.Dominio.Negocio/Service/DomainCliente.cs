﻿using ConsultaFIPE.Dominio.Entidade.Enum;
using ConsultaFIPE.Dominio.Entidade.Model;
using ConsultaFIPE.Dominio.Negocio.Contract.Repository;
using ConsultaFIPE.Dominio.Negocio.Contract.Services;
using Microsoft.EntityFrameworkCore;
using Sigtrans.Uranus.Net.UranusCore.Dominio.Usuario.Negocio.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Negocio.Service
{
    public class DomainCliente : DomainBase<Cliente>, IDomainCliente
    {
        private readonly IRepositoryCliente _repositoryCliente;
        private readonly IRepositoryHistoricoAtualizacaoCliente _repositoryHistoricoAtualizacaoCliente;

        public DomainCliente(IRepositoryCliente repositoryCliente, IRepositoryHistoricoAtualizacaoCliente repositoryHistoricoAtualizacaoCliente)
            : base(repositoryCliente)
        {
            _repositoryCliente = repositoryCliente;
            _repositoryHistoricoAtualizacaoCliente = repositoryHistoricoAtualizacaoCliente;
        }

        /// <summary>
        /// Método que atualiza um cliente
        /// </summary>
        /// <param name="cliente">Cliente a ser atualizado</param>
        /// <returns></returns>
        public override async Task Atualizar(Cliente cliente)
        {
            try
            {
                Cliente clienteCadastrado = await _repositoryCliente.FindAllbyExpression(f => f.UUID == cliente.UUID).FirstOrDefaultAsync();

                #region Validação

                if (clienteCadastrado == null)
                {
                    throw new Exception("Cliente não cadastrado.");
                }

                ClienteCicloUso cicloUsoAtivo = clienteCadastrado.CiclosUso.Where(f => f.Status == (int)StatusEnum.ClienteCicloUso.Ativo).FirstOrDefault();

                if(cicloUsoAtivo != null && !cliente.Ativo)
                {
                    throw new Exception(String.Format("O Cliente '{0}' possui Plano Ativo para Consulta de Veículo. Cancele o mesmo ou aguarde a expiração antes de inativar o Cliente.", cliente.Nome));
                }

                #endregion

                #region Histórico Atualização

                HistoricoAtualizacaoCliente historicoAtualizacaoCliente = new HistoricoAtualizacaoCliente()
                {
                    AtivoAntigo = clienteCadastrado.Ativo,
                    AtivoNovo = cliente.Ativo,
                    DataAtualizacao = DateTime.Now,
                    IdCliente = clienteCadastrado.IdCliente,
                    Login = cliente.Login,
                    NomeAntigo = clienteCadastrado.Nome,
                    NomeNovo = cliente.Nome,
                    TempoVigenciaAntigo = clienteCadastrado.TempoVigenciaCacheVeiculo,
                    TempoVigenciaNovo = cliente.TempoVigenciaCacheVeiculo
                };

                await _repositoryHistoricoAtualizacaoCliente.Insert(historicoAtualizacaoCliente);

                #endregion

                clienteCadastrado.Ativo = cliente.Ativo;
                clienteCadastrado.Nome = cliente.Nome;
                clienteCadastrado.TempoVigenciaCacheVeiculo = cliente.TempoVigenciaCacheVeiculo;

                await base.Atualizar(clienteCadastrado);
            }

            catch
            {
                throw;
            }


        }
    }
}
