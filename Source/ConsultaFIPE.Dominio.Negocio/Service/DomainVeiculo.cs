﻿using ConsultaFIPE.Dominio.Entidade.Model;
using ConsultaFIPE.Dominio.Negocio.Contract.Repository;
using ConsultaFIPE.Dominio.Negocio.Contract.Services;
using Sigtrans.Uranus.Net.UranusCore.Dominio.Usuario.Negocio.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Negocio.Service
{
    public class DomainVeiculo : DomainBase<Veiculo>, IDomainVeiculo
    {
        private readonly IRepositoryVeiculo _repositoryVeiculo;

        public DomainVeiculo(IRepositoryVeiculo repositoryVeiculo)
            : base(repositoryVeiculo)
        {
            _repositoryVeiculo = repositoryVeiculo;
        }
    }
}
