﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Infraestrutura.Utilidades.Cliente
{
    public interface IWorkflowClienteValidation
    {
		/// <summary>
		/// Método que verifica se o CNPJ é válido ou não
		/// </summary>
		/// <param name="cnpj">CNPJ</param>
		/// <returns>true: válido, false: inválido</returns>
		bool CNPJValido(string cnpj);

		/// <summary>
		/// Método que verifica se o CPF é válido ou não
		/// </summary>
		/// <param name="cpf">CPF</param>
		/// <returns>true: válido, false: inválido</returns>
		bool CPFValido(string cpf);

	}
}
