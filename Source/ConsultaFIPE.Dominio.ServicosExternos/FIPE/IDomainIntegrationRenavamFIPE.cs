﻿using ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.ServicosExternos.FIPE
{
    public interface IDomainIntegrationRenavamFIPE
    {
        /// <summary>
        /// Método que lista o veículo por placa
        /// </summary>
        /// <param name="placa">Placa</param>
        /// <param name="login">Login</param>
        /// <param name="cliente">Cliente</param>
        /// <param name="identificador">Identificador</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="longitude">Longitude</param>
        /// <returns>Veículo</returns>
        Task<VeiculoFIPE> ListarVeiculoPorPlaca(string placa, string login, string cliente, string identificador, string latitude, string longitude);
    }
}
