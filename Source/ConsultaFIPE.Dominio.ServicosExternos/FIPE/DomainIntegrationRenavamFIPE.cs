﻿using ConsultaFIPE.Dominio.Entidade.Enum;
using ConsultaFIPE.Dominio.Entidade.Model;
using ConsultaFIPE.Dominio.Negocio.Contract.Repository;
using ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.FIPE;
using ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.Model;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.ServicosExternos.FIPE
{
    public class DomainIntegrationRenavamFIPE : IDomainIntegrationRenavamFIPE
    {
        private readonly IIntegrationRenavamFIPE _integrationRenavamFIPE;
        private readonly IRepositoryCliente _repositoryCliente;
        private readonly IRepositoryClienteCicloUso _repositoryClienteCicloUso;
        private readonly IRepositoryLog _repositoryLog;
        private readonly IRepositoryVeiculo _repositoryVeiculo;

        private int TipoConsuta { get; set; } = 1;

        public DomainIntegrationRenavamFIPE(IIntegrationRenavamFIPE integrationRenavamFIPE, IRepositoryCliente repositoryCliente, IRepositoryClienteCicloUso repositoryClienteCicloUso,
                                            IRepositoryLog repositoryLog, IRepositoryVeiculo repositoryVeiculo)
        {
            _integrationRenavamFIPE = integrationRenavamFIPE;
            _repositoryCliente = repositoryCliente;
            _repositoryClienteCicloUso = repositoryClienteCicloUso;
            _repositoryLog = repositoryLog;
            _repositoryVeiculo = repositoryVeiculo;
        }

        /// <summary>
        /// Método que lista o veículo por placa
        /// </summary>
        /// <param name="placa">Placa</param>
        /// <param name="login">Login</param>
        /// <param name="cliente">Cliente</param>
        /// <param name="identificador">Identificador</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="longitude">Longitude</param>
        /// <returns>Veículo</returns>
        public async Task<VeiculoFIPE> ListarVeiculoPorPlaca(string placa, string login, string cliente, string identificador, string latitude, string longitude)
        {
            Guid clienteGUID;
            Guid.TryParse(cliente, out clienteGUID);

            Cliente clienteCadastro = _repositoryCliente.FindAllbyExpression(f => f.Alias == cliente || f.UUID == clienteGUID).FirstOrDefault();

            VeiculoFIPE veiculoFIPE = null;

            Log logErro = null;

            try
            {
                //Cliente nulo
                if (clienteCadastro == null)
                {
                    throw new Exception(String.Format("Cliente '{0}' não cadastrado.", cliente));
                }

                else
                {
                    logErro = new Log()
                    {
                        Identificador = identificador,
                        Data = DateTime.Now,
                        Latitude = latitude,
                        Longitude = longitude,
                        Tipo = (int)TipoEnum.Log.Erro,
                        Usuario = login,
                        IdCliente = clienteCadastro.IdCliente,
                    };

                    //Cliente inativo
                    if (!clienteCadastro.Ativo)
                    {
                        logErro.Valor = String.Format("Cliente '{0}' inativo.", clienteCadastro.Nome);
                        await _repositoryLog.Insert(logErro);

                        throw new Exception(logErro.Valor);
                    }

                    ClienteCicloUso cicloUsoAtivo = clienteCadastro.CiclosUso.FirstOrDefault(f => f.Status == (int)StatusEnum.ClienteCicloUso.Ativo);

                    //Sem ciclo de uso ativo
                    if (cicloUsoAtivo == null)
                    {
                        logErro.Valor = String.Format("Cliente '{0}' sem Plano Ativo para Consulta de Veículo.", clienteCadastro.Nome);
                        await _repositoryLog.Insert(logErro);

                        throw new Exception(logErro.Valor);
                    }

                    else
                    {
                        //Ciclo de uso expirado
                        if (cicloUsoAtivo.DataFim < DateTime.Now)
                        {
                            //Caso haja renovação automática, gere um novo ciclo de uso para aquele cliente
                            if (cicloUsoAtivo.RenovacaoAuto)
                            {
                                veiculoFIPE = await BuscarVeiculo(placa, clienteCadastro.TempoVigenciaCacheVeiculo, identificador, login, latitude, longitude, clienteCadastro.IdCliente);

                                cicloUsoAtivo.Status = (int)StatusEnum.ClienteCicloUso.Expirado;
                                await _repositoryClienteCicloUso.Update(cicloUsoAtivo);

                                ClienteCicloUso cicloUsoRenovacao = new ClienteCicloUso()
                                {
                                    DataInicio = DateTime.Now,
                                    DataFim = DateTime.Now.AddDays(cicloUsoAtivo.TempoVigencia),
                                    IdCliente = cicloUsoAtivo.IdCliente,
                                    QtdAcessoDisponivel = cicloUsoAtivo.QtdAcessoDisponivel,
                                    QtdAcessoTotal = TipoConsuta == (int)TipoEnum.Log.Webservice ? 1 : 0,
                                    RenovacaoAuto = cicloUsoAtivo.RenovacaoAuto,
                                    Status = (int)StatusEnum.ClienteCicloUso.Ativo,
                                    TempoVigencia = cicloUsoAtivo.TempoVigencia
                                };

                                await _repositoryClienteCicloUso.Insert(cicloUsoRenovacao);
                            }

                            else
                            {
                                cicloUsoAtivo.Status = (int)StatusEnum.ClienteCicloUso.Expirado;
                                await _repositoryClienteCicloUso.Update(cicloUsoAtivo);

                                logErro.Valor = String.Format("O Plano de Consulta de Veículo do Cliente '{0}' expirou em '{1}'.",
                                                              clienteCadastro.Nome, cicloUsoAtivo.DataFim.ToString("dd/MM/yyyy HH:mm"));

                                await _repositoryLog.Insert(logErro);

                                throw new Exception(logErro.Valor);
                            }
                        }

                        else
                        {
                            if (cicloUsoAtivo.QtdAcessoTotal <= cicloUsoAtivo.QtdAcessoDisponivel)
                            {
                                veiculoFIPE = await BuscarVeiculo(placa, clienteCadastro.TempoVigenciaCacheVeiculo, identificador, login, latitude, longitude, clienteCadastro.IdCliente);

                                cicloUsoAtivo.QtdAcessoTotal = TipoConsuta == (int)TipoEnum.Log.Webservice ? cicloUsoAtivo.QtdAcessoTotal + 1 : cicloUsoAtivo.QtdAcessoTotal;
                                await _repositoryClienteCicloUso.Update(cicloUsoAtivo);
                            }

                            //Quantidade de uso máxima alcançada
                            else
                            {
                                logErro.Valor = String.Format("Quantidade de Uso Máximo do Plano de Consulta de Veículo alcançada para o Cliente '{0}'.", clienteCadastro.Nome);
                                await _repositoryLog.Insert(logErro);

                                throw new Exception(logErro.Valor);
                            }
                        }
                    }
                }
            }

            catch
            {
                throw;
            }

            return veiculoFIPE;
        }

        /// <summary>
        /// Método que realiza a busca do veículo no cache ou no webservice.
        /// </summary>
        /// <param name="placa">Placa</param>
        /// <param name="tempoVigenciaCacheVeiculo">Tempo de Vigência do cache do veículo</param>
        /// <param name="identificador">Identificado</param>
        /// <param name="login">Login</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="longitude">Longitude</param>
        /// <param name="idCliente">Id Cliente</param>
        /// <returns>Veículo</returns>
        private async Task<VeiculoFIPE> BuscarVeiculo(string placa, int tempoVigenciaCacheVeiculo, string identificador, string login, string latitude, string longitude, int idCliente)
        {
            Veiculo veiculoCache = await _repositoryVeiculo.FindAllbyExpression(f => f.Placa == placa).FirstOrDefaultAsync();
            VeiculoFIPE veiculoFIPE = null;

            Log log = new Log()
            {
                Data = DateTime.Now,
                IdCliente = idCliente,
                Identificador = identificador,
                Latitude = latitude,
                Longitude = longitude,
                Usuario = login
            };

            if (veiculoCache == null)
            {
                veiculoFIPE = await _integrationRenavamFIPE.ListarVeiculoPorPlaca(placa);

                veiculoCache = new Veiculo()
                {
                    Ano = veiculoFIPE.Ano,
                    CapacidadeCarga = veiculoFIPE.CapacidadeCarga,
                    Chassi = veiculoFIPE.Chassi,
                    Cilindrada = veiculoFIPE.Cilindrada,
                    CMT = veiculoFIPE.CMT,
                    Combustivel = veiculoFIPE.Combustivel,
                    Cor = veiculoFIPE.Cor,
                    DataUltimaConsulta = DateTime.Now,
                    MarcaModelo = veiculoFIPE.MarcaModelo,
                    Motor = veiculoFIPE.Motor,
                    Municipio = veiculoFIPE.Municipio,
                    PBT = veiculoFIPE.PBT,
                    Placa = veiculoFIPE.Placa,
                    Potencia = veiculoFIPE.Potencia,
                    Procedencia = veiculoFIPE.Procedencia,
                    QtdEixo = veiculoFIPE.QtdEixo.HasValue ? veiculoFIPE.QtdEixo.ToString() : null,
                    QtdPassageiro = veiculoFIPE.QtdPassageiro.HasValue ? veiculoFIPE.QtdPassageiro.ToString() : null,
                    Tipo = veiculoFIPE.Tipo,
                    UF = veiculoFIPE.UF
                };

                log.Tipo = TipoConsuta = (int)TipoEnum.Log.Webservice;
                await _repositoryVeiculo.Insert(veiculoCache);
            }

            else
            {
                //Caso o cache do veículo ainda esteja válido, use o mesmo
                if (tempoVigenciaCacheVeiculo > 0 && DateTime.Now < veiculoCache.DataUltimaConsulta.Value.AddDays(tempoVigenciaCacheVeiculo))
                {
                    veiculoFIPE = new VeiculoFIPE()
                    {
                        Ano = veiculoCache.Ano,
                        CapacidadeCarga = veiculoCache.CapacidadeCarga,
                        Chassi = veiculoCache.Chassi,
                        Cilindrada = veiculoCache.Cilindrada,
                        CMT = veiculoCache.CMT,
                        Combustivel = veiculoCache.Combustivel,
                        Cor = veiculoCache.Cor,
                        MarcaModelo = veiculoCache.MarcaModelo,
                        Motor = veiculoCache.Motor,
                        Municipio = veiculoCache.Municipio,
                        PBT = veiculoCache.PBT,
                        Placa = veiculoCache.Placa,
                        Potencia = veiculoCache.Potencia,
                        Procedencia = veiculoCache.Procedencia,
                        QtdEixo = !String.IsNullOrEmpty(veiculoCache.QtdEixo) ? Convert.ToInt32(veiculoCache.QtdEixo) : null,
                        QtdPassageiro = !String.IsNullOrEmpty(veiculoCache.QtdPassageiro) ? Convert.ToInt32(veiculoCache.QtdPassageiro) : null,
                        Tipo = veiculoCache.Tipo,
                        UF = veiculoCache.UF,
                    };

                    log.Tipo = TipoConsuta = (int)TipoEnum.Log.Cache;
                }

                else
                {
                    veiculoFIPE = await _integrationRenavamFIPE.ListarVeiculoPorPlaca(placa);

                    veiculoCache.Ano = veiculoFIPE.Ano;
                    veiculoCache.Placa = veiculoFIPE.Placa;
                    veiculoCache.UF = veiculoFIPE.UF;
                    veiculoCache.Motor = veiculoFIPE.Motor;
                    veiculoCache.CMT = veiculoFIPE.CMT;
                    veiculoCache.Chassi = veiculoFIPE.Chassi;
                    veiculoCache.CapacidadeCarga = veiculoFIPE.CapacidadeCarga;
                    veiculoCache.Cilindrada = veiculoFIPE.Cilindrada;
                    veiculoCache.Combustivel = veiculoFIPE.Combustivel;
                    veiculoCache.Cor = veiculoFIPE.Cor;
                    veiculoCache.MarcaModelo = veiculoFIPE.MarcaModelo;
                    veiculoCache.Municipio = veiculoFIPE.Municipio;
                    veiculoCache.PBT = veiculoFIPE.PBT;
                    veiculoCache.Potencia = veiculoFIPE.Potencia;
                    veiculoCache.Procedencia = veiculoFIPE.Procedencia;
                    veiculoCache.QtdEixo = veiculoFIPE.QtdEixo.HasValue ? veiculoFIPE.QtdEixo.ToString() : null;
                    veiculoCache.QtdPassageiro = veiculoFIPE.QtdPassageiro.HasValue ? veiculoFIPE.QtdPassageiro.ToString() : null;
                    veiculoCache.Tipo = veiculoFIPE.Tipo;

                    log.Tipo = TipoConsuta = (int)TipoEnum.Log.Webservice;
                }

                //Atualizo a data de última consulta
                veiculoCache.DataUltimaConsulta = DateTime.Now;
                await _repositoryVeiculo.Update(veiculoCache);

                //Inserir Log
                log.Valor = JsonConvert.SerializeObject(veiculoFIPE);
                await _repositoryLog.Insert(log);
            }

            return veiculoFIPE;
        }
    }
}
