﻿using ConsultaFIPE.Dominio.Entidade.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Infraestrutura.RepositorioEF.Configuration
{
    public class HistoricoAtualizacaoClienteConfiguration : IEntityTypeConfiguration<HistoricoAtualizacaoCliente>
    {
        /// <summary>
        /// Método que mapeia os atributos da classe na tabela
        /// </summary>
        /// <param name="builder">Construtor do Mapeamento</param>
        public void Configure(EntityTypeBuilder<HistoricoAtualizacaoCliente> builder)
        {
            builder.ToTable("HST_ATUALIZACAO_CLIENTE");

            builder.HasKey(m => m.IdHistoricoAtualizacaoCliente);

            builder.Property(m => m.IdHistoricoAtualizacaoCliente)
                   .HasColumnName("ID_HST_ATUALIZACAO_CLIENTE")
                   .ValueGeneratedOnAdd()
                   .IsRequired();

            builder.Property(m => m.Login)
            .HasColumnName("NM_LOGIN")
            .IsRequired();

            builder.Property(m => m.DataAtualizacao)
            .HasColumnName("DTH_ATUALIZACAO")
            .IsRequired();

            builder.Property(m => m.NomeAntigo)
            .HasColumnName("NM_ANTIGO")
            .IsRequired();

            builder.Property(m => m.NomeNovo)
            .HasColumnName("NM_NOVO")
            .IsRequired();

            builder.Property(m => m.TempoVigenciaAntigo)
            .HasColumnName("TEMPO_VIGENCIA_ANTIGO")
            .IsRequired();

            builder.Property(m => m.TempoVigenciaNovo)
            .HasColumnName("TEMPO_VIGENCIA_NOVO")
            .IsRequired();

            builder.Property(m => m.AtivoAntigo)
            .HasColumnName("ATIVO_ANTIGO")
            .IsRequired();

            builder.Property(m => m.AtivoNovo)
            .HasColumnName("ATIVO_NOVO")
            .IsRequired();

            //Relacionamento Many-to-one
            builder.Property(m => m.IdCliente)
                   .HasColumnName("ID_CLIENTE")
                   .IsRequired();

            builder.HasOne(c => c.Cliente)
                   .WithMany()
                   .HasForeignKey(c => c.IdCliente);
        }
    }
}
