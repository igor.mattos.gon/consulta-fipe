﻿using ConsultaFIPE.Dominio.Entidade.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Infraestrutura.RepositorioEF.Configuration
{
    public class VeiculoConfiguration : IEntityTypeConfiguration<Veiculo>
    {
        /// <summary>
        /// Método que mapeia os atributos da classe na tabela
        /// </summary>
        /// <param name="builder">Construtor do Mapeamento</param>
        public void Configure(EntityTypeBuilder<Veiculo> builder)
        {
            builder.ToTable("VEICULO");

            builder.HasKey(m => m.IdVeiculo);

            builder.Property(m => m.IdVeiculo)
                   .HasColumnName("ID_VEICULO")
                   .ValueGeneratedOnAdd()
                   .IsRequired();

            builder.Property(m => m.UF)
            .HasColumnName("UF")
            .IsRequired(false);

            builder.Property(m => m.Ano)
            .HasColumnName("ANO")
            .IsRequired(false);

            builder.Property(m => m.CMT)
            .HasColumnName("CMT")
            .IsRequired(false);

            builder.Property(m => m.Cor)
            .HasColumnName("COR")
            .IsRequired(false);

            builder.Property(m => m.PBT)
            .HasColumnName("PBT")
            .IsRequired(false);

            builder.Property(m => m.Placa)
            .HasColumnName("PLACA")
            .IsRequired();

            builder.Property(m => m.Chassi)
            .HasColumnName("CHASSI")
            .IsRequired();

            builder.Property(m => m.Motor)
            .HasColumnName("MOTOR")
            .IsRequired(false);

            builder.Property(m => m.Potencia)
            .HasColumnName("POTENCIA")
            .IsRequired(false);

            builder.Property(m => m.Municipio)
            .HasColumnName("MUNICIPIO")
            .IsRequired(false);            

            builder.Property(m => m.Cilindrada)
            .HasColumnName("CILINDRADA")
            .IsRequired(false);

            builder.Property(m => m.Combustivel)
            .HasColumnName("COMBUSTIVEL")
            .IsRequired(false);

            builder.Property(m => m.Procedencia)
            .HasColumnName("PROCEDENCIA")
            .IsRequired(false);

            builder.Property(m => m.MarcaModelo)
            .HasColumnName("MARCA_MODELO")
            .IsRequired();

            builder.Property(m => m.Tipo)
            .HasColumnName("TIPO")
            .IsRequired(false);

            builder.Property(m => m.CapacidadeCarga)
            .HasColumnName("CAPACIDADE_CARGA")
            .IsRequired(false);

            builder.Property(m => m.QtdEixo)
            .HasColumnName("QTD_EIXO")
            .IsRequired(false);

            builder.Property(m => m.QtdPassageiro)
            .HasColumnName("QTD_PASSAGEIRO")
            .IsRequired(false);

            builder.Property(m => m.DataUltimaConsulta)
            .HasColumnName("DTH_ULTIMA_CONSULTA")
            .IsRequired(false);
        }
    }
}
