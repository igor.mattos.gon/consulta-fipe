﻿using ConsultaFIPE.Dominio.Entidade.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Infraestrutura.RepositorioEF.Configuration
{
    public class LogConfiguration : IEntityTypeConfiguration<Log>
    {
        /// <summary>
        /// Método que mapeia os atributos da classe na tabela
        /// </summary>
        /// <param name="builder">Construtor do Mapeamento</param>
        public void Configure(EntityTypeBuilder<Log> builder)
        {
            builder.ToTable("LOG");

            builder.HasKey(m => m.IdLog);

            builder.Property(m => m.IdLog)
                   .HasColumnName("ID_LOG")
                   .ValueGeneratedOnAdd()
                   .IsRequired();

            builder.Property(m => m.Usuario)
            .HasColumnName("USUARIO")
            .IsRequired();

            builder.Property(m => m.Identificador)
            .HasColumnName("IDENTIFICADOR")
            .IsRequired();

            builder.Property(m => m.Data)
            .HasColumnName("DTH_CADASTRO")
            .IsRequired();

            builder.Property(m => m.Latitude)
            .HasColumnName("LATITUDE")
            .IsRequired(false);

            builder.Property(m => m.Longitude)
            .HasColumnName("LONGITUDE")
            .IsRequired(false);

            builder.Property(m => m.Tipo)
            .HasColumnName("TIPO")
            .IsRequired();

            builder.Property(m => m.Valor)
            .HasColumnName("VALOR")
            .IsRequired();

            //Relacionamento Many-to-one
            builder.Property(m => m.IdCliente)
                   .HasColumnName("ID_CLIENTE")
                   .IsRequired();

            builder.HasOne(c => c.Cliente)
                   .WithMany()
                   .HasForeignKey(c => c.IdCliente);
        }
    }
}
