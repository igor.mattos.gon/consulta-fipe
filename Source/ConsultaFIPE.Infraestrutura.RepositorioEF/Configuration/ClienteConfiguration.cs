﻿using ConsultaFIPE.Dominio.Entidade.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Sigtrans.Uranus.Net.UranusCore.Infraestrutura.RepositorioEF.Configuration
{
    public class ClienteConfiguration : IEntityTypeConfiguration<Cliente>
    {
        /// <summary>
        /// Método que mapeia os atributos da classe na tabela
        /// </summary>
        /// <param name="builder">Construtor do Mapeamento</param>
        public void Configure(EntityTypeBuilder<Cliente> builder)
        {
            builder.ToTable("CLIENTE");

            builder.HasKey(m => m.IdCliente);

            builder.Property(m => m.IdCliente)
                   .HasColumnName("ID_CLIENTE")
                   .ValueGeneratedOnAdd()
                   .IsRequired();

            builder.Property(m => m.UUID)
            .HasColumnName("UNIQUE_ID")            
            .IsRequired();

            builder.Property(m => m.Nome)
            .HasColumnName("NM_CLIENTE")
            .IsRequired();

            builder.Property(m => m.Alias)
            .HasColumnName("ALIAS")            
            .IsRequired();

            builder.Property(m => m.TempoVigenciaCacheVeiculo)
            .HasColumnName("TEMPO_VIGENCIA_CACHE_VEICULO")
            .IsRequired();

            builder.Property(m => m.Ativo)
            .HasColumnName("ATIVO")
            .IsRequired();

            builder.Property(m => m.DataCadastro)
            .HasColumnName("DTH_CADASTRO")
            .IsRequired();

            //Relacionamentos One-to-Many
            builder.HasMany(c => c.CiclosUso).WithOne(c => c.Cliente).HasForeignKey(c => c.IdCliente);
        }
    }
}
