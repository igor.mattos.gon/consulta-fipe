﻿using ConsultaFIPE.Dominio.Entidade.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Infraestrutura.RepositorioEF.Configuration
{
    public class ClienteCicloUsoConfiguration : IEntityTypeConfiguration<ClienteCicloUso>
    {
        /// <summary>
        /// Método que mapeia os atributos da classe na tabela
        /// </summary>
        /// <param name="builder">Construtor do Mapeamento</param>
        public void Configure(EntityTypeBuilder<ClienteCicloUso> builder)
        {
            builder.ToTable("CLIENTE_CICLO_USO");

            builder.HasKey(m => m.IdClienteClicloUso);

            builder.Property(m => m.IdClienteClicloUso)
                   .HasColumnName("ID_CLIENTE_CICLO_USO")
                   .ValueGeneratedOnAdd()
                   .IsRequired();

            builder.Property(m => m.DataInicio)
            .HasColumnName("DTH_INICIO")
            .IsRequired();

            builder.Property(m => m.DataFim)
            .HasColumnName("DTH_FIM")
            .IsRequired();

            builder.Property(m => m.QtdAcessoDisponivel)
            .HasColumnName("QTD_ACESSO")
            .IsRequired();

            builder.Property(m => m.QtdAcessoTotal)
            .HasColumnName("QTD_ACESSO_TOTAL")
            .IsRequired();

            builder.Property(m => m.TempoVigencia)
            .HasColumnName("TEMPO_VIGENCIA")
            .IsRequired();

            builder.Property(m => m.RenovacaoAuto)
            .HasColumnName("RENOVACAO_AUTO")
            .IsRequired();

            builder.Property(m => m.Status)
            .HasColumnName("SITUACAO")
            .IsRequired();

            builder.Property(m => m.Observacao)
            .HasColumnName("OBSERVACAO")
            .IsRequired(false);

            //Relacionamento Many-to-one
            builder.Property(m => m.IdCliente)
                   .HasColumnName("ID_CLIENTE")
                   .IsRequired();

            builder.HasOne(c => c.Cliente)
                   .WithMany(c => c.CiclosUso)
                   .HasForeignKey(c => c.IdCliente);
        }
    }
}
