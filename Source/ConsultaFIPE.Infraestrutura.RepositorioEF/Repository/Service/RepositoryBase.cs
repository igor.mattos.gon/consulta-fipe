﻿using Sigtrans.Uranus.Net.UranusCore.Infraestrutura.RepositorioEF.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Sigtrans.Uranus.Net.UranusCore.Dominio.Usuario.Negocio.Contract.Repository;

namespace Sigtrans.Uranus.Net.UranusCore.Infraestrutura.RepositorioEF.Repository.Service
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly DataContext _context;

        /// <summary>
        /// Construtor da Classe
        /// </summary>
        public RepositoryBase(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Busca um objeto na base a partir de um Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TEntity> FindById(int id)
        {
            try
            {
                return await _context.Set<TEntity>().FindAsync(id);
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Busca um objeto na base a partir de um codigo
        /// </summary>
        /// <param name="cod"></param>
        /// <returns></returns>
        public TEntity FindByCode(string cod)
        {
            try
            {
                return _context.Set<TEntity>().Find(cod);
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Lista os objetos na base
        /// </summary>
        /// <returns></returns>
        public IQueryable<TEntity> FindAll()
        {
            try
            {
                return from c in _context.Set<TEntity>() select c;
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Insere um objeto na base
        /// </summary>
        /// <param name="item"></param>
        public async Task Insert(TEntity item)
        {
            try
            {
                _context.Set<TEntity>().Add(item);
                await _context.SaveChangesAsync();
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Exclui um objeto da base
        /// </summary>
        /// <param name="item"></param>
        public void Delete(TEntity item)
        {
            try
            {
                _context.Set<TEntity>().Remove(item);
                _context.SaveChanges();
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Exclui um objeto da base
        /// </summary>
        /// <param name="item"></param>
        public void Delete(int id)
        {
            try
            {
                var result = _context.Set<TEntity>().Find(id);

                _context.Set<TEntity>().Remove(result);
                _context.SaveChanges();
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Altera um objeto da base
        /// </summary>
        /// <param name="item"></param>
        public async Task Update(TEntity item)
        {
            try
            {
                _context.Entry(item).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception) { throw; }

        }

        /// <summary>
        /// Liberar os recursos instanciados pela classe DbContext
        /// </summary>
        public void Dispose()
        {
            _context.Dispose();
        }

        /// <summary>
        /// Lista os objetos na base baseado em uma expressão exemplo: c => c.ativo == true
        /// </summary>
        /// <returns></returns>
        public IQueryable<TEntity> FindAllbyExpression(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                return _context.Set<TEntity>().Where(predicate);
            }
            catch (Exception) { throw; }
        }
    }
}
