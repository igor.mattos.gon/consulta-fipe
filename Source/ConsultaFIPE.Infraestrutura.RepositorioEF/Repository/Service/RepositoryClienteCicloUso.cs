﻿using ConsultaFIPE.Dominio.Entidade.Model;
using ConsultaFIPE.Dominio.Negocio.Contract.Repository;
using Sigtrans.Uranus.Net.UranusCore.Infraestrutura.RepositorioEF.Context;
using Sigtrans.Uranus.Net.UranusCore.Infraestrutura.RepositorioEF.Repository.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Infraestrutura.RepositorioEF.Repository.Service
{
    public class RepositoryClienteCicloUso : RepositoryBase<ClienteCicloUso>, IRepositoryClienteCicloUso
    {
        public RepositoryClienteCicloUso(DataContext dataContext) 
            : base(dataContext)
        {
        }
    }
}
