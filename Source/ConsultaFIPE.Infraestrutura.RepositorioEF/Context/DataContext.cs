﻿using Sigtrans.Uranus.Net.UranusCore.Infraestrutura.RepositorioEF.Configuration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsultaFIPE.Infraestrutura.RepositorioEF.Configuration;

namespace Sigtrans.Uranus.Net.UranusCore.Infraestrutura.RepositorioEF.Context
{
    public class DataContext : DbContext
    {

        public DataContext(DbContextOptions<DataContext> options)
                : base(options)
        {

        }

        /// <summary>
        /// Método que adiciona os arquivos de mapeamento de cada classe
        /// </summary>
        /// <param name="modelBuilder">Construtor do Modelo</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configuração
            modelBuilder.ApplyConfiguration(new ClienteConfiguration());
            modelBuilder.ApplyConfiguration(new ClienteCicloUsoConfiguration());
            modelBuilder.ApplyConfiguration(new HistoricoAtualizacaoClienteConfiguration());
            modelBuilder.ApplyConfiguration(new LogConfiguration());
            modelBuilder.ApplyConfiguration(new VeiculoConfiguration());
        }
    }
}
