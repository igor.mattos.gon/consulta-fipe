﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace ConsultaFIPE.Apresentacao.WebApi.Filters
{
    public class CustomParameterFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
            {
                operation.Parameters = new List<OpenApiParameter>();
            }

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "cliente",
                In = ParameterLocation.Header,
                Description = "Id do Cliente"                
            });

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "identificador",
                In = ParameterLocation.Header,
                Description = "Identificador"
            });

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "login",
                In = ParameterLocation.Header,
                Description = "Login do Usuário"
            });

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "latitude",
                In = ParameterLocation.Header,
                Description = "Latitude"
            });

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "longitude",
                In = ParameterLocation.Header,
                Description = "longitude"
            });
        }
    }
}
