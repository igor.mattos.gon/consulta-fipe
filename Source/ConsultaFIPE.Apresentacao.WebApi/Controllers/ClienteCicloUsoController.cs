﻿using AutoMapper;
using ConsultaFIPE.Apresentacao.WebApi.Filters;
using ConsultaFIPE.Apresentacao.WebApi.Model.Exception;
using ConsultaFIPE.Apresentacao.WebApi.Model.Response;
using ConsultaFIPE.Apresentacao.WebApi.Model;
using ConsultaFIPE.Dominio.Entidade.Model;
using ConsultaFIPE.Dominio.Negocio.Contract.Services;
using ConsultaFIPE.Dominio.Negocio.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ConsultaFIPE.Apresentacao.WebApi.Controllers
{
    [Route("api/ciclo-uso")]
    [ApiController]
    [ValidateModel]
    public class ClienteCicloUsoController : ControllerBase
    {
        private readonly IDomainClienteCicloUso _domainClienteCicloUso;
        private readonly IMapper _mapper;

        public ClienteCicloUsoController(IDomainClienteCicloUso domainClienteCicloUso, IMapper mapper)
        {
            _domainClienteCicloUso = domainClienteCicloUso;
            _mapper = mapper;
        }

        /// <summary>
        /// API de cadastro
        /// </summary>
        /// <param name="cliente">Objeto Cliente Ciclo Uso</param>
        /// <returns>Cliente Ciclo Uso cadastrado</returns>
        [HttpPost()]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Post(ClienteCicloUsoModelCadastro cicloUso)
        {
            try
            {
                ClienteCicloUso cicloUsoCadastro = _mapper.Map<ClienteCicloUsoModelCadastro, ClienteCicloUso>(cicloUso);
                cicloUsoCadastro.Cliente = new Cliente() { UUID = Guid.Parse(cicloUso.UUIDCliente) };

                await _domainClienteCicloUso.Cadastrar(cicloUsoCadastro);

                return Ok(ResponseFactory<ClienteCicloUsoModelCadastro>.GerarResponse(cicloUso));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

        /// <summary>
        /// API de atualzação
        /// </summary>
        /// <param name="cicloUsoCancelamento">Objeto Cliente Ciclo Uso</param>
        /// <returns></returns>
        [HttpPut("cancelar")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Put(ClienteCicloUsoModelCancelamento cicloUsoCancelamento)
        {
            try
            {
                await _domainClienteCicloUso.CancelarCicloUso(cicloUsoCancelamento.IdClienteClicloUso, cicloUsoCancelamento.Login);
                return Ok(ResponseFactory<string>.GerarResponse("Plano de Consulta de Veículo cancelado com sucesso."));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

        /// <summary>
        /// Metodo de listagem de ciclos de uso por cliente
        /// </summary>                        
        /// <returns>Lista de Ciclo de Uso</returns>
        [HttpGet("consulta-por-cliente/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetByCliente(string id)
        {
            try
            {
                Guid guidCliente = Guid.Parse(id);

                IEnumerable<ClienteCicloUso> ciclosUso = await _domainClienteCicloUso.ListarporExpressao(f => f.Cliente.UUID == guidCliente)
                                                                                     .ToListAsync();

                IEnumerable<ClienteCicloUsoListagem> cicloUsoListagem = _mapper.Map<IEnumerable<ClienteCicloUso>, IEnumerable<ClienteCicloUsoListagem>>(ciclosUso);

                if (cicloUsoListagem.Count() == 0)
                {
                    return NotFound(ResponseFactory<string>.GerarResponse("Nenhum plano de consulta foi encontrado para o cliente."));
                }

                return Ok(ResponseFactory<IEnumerable<ClienteCicloUsoListagem>>.GerarResponse(cicloUsoListagem));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }
    }
}
