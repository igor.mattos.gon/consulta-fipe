﻿using ConsultaFIPE.Dominio.ServicosExternos.FIPE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using System.Threading.Tasks;
using System;
using ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.Model;
using ConsultaFIPE.Apresentacao.WebApi.Model.Response;
using ConsultaFIPE.Apresentacao.WebApi.Model.Exception;
using System.Linq;
using Microsoft.Extensions.Primitives;
using ConsultaFIPE.Dominio.Negocio.Contract.Services;
using ConsultaFIPE.Dominio.Entidade.Model;

namespace ConsultaFIPE.Apresentacao.WebApi.Controllers
{
    [Route("api/veiculo")]
    [ApiController]
    public class VeiculoController : ControllerBase
    {
        private readonly IDomainIntegrationRenavamFIPE _domainIntegrationRenavamFIPE;        

        public VeiculoController(IDomainIntegrationRenavamFIPE domainIntegrationRenavamFIPE)
        {
            _domainIntegrationRenavamFIPE = domainIntegrationRenavamFIPE;
        }

        /// <summary>
        /// Método que busca o veículo por placa
        /// </summary>
        /// <param name="placa">Placa</param>
        /// <returns>Veículo</returns>       
        [HttpGet("consulta-por-placa/{placa}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> GetByPlaca(string placa)
        {
            try
            {
                StringValues cliente = String.Empty;
                StringValues login = String.Empty;
                StringValues identificador = String.Empty;
                StringValues latitude = String.Empty;
                StringValues longitude = String.Empty;

                #region Validação Cabeçalho

                if (!HttpContext.Request.Headers.TryGetValue("cliente", out cliente))
                {
                    return BadRequest(ResponseFactory<string>.GerarResponse("Cliente não encontrado."));
                }

                if (!HttpContext.Request.Headers.TryGetValue("identificador", out identificador))
                {
                    return BadRequest(ResponseFactory<string>.GerarResponse("Identificador não encontrado."));
                }

                if (!HttpContext.Request.Headers.TryGetValue("login", out login))
                {
                    return BadRequest(ResponseFactory<string>.GerarResponse("Usuário não encontrado."));
                }

                HttpContext.Request.Headers.TryGetValue("latitude", out latitude);
                HttpContext.Request.Headers.TryGetValue("longitude", out longitude);

                #endregion

                VeiculoFIPE veiculoFIPE = await _domainIntegrationRenavamFIPE.ListarVeiculoPorPlaca(placa, login, cliente, identificador, latitude, longitude);
                return Ok(ResponseFactory<VeiculoFIPE>.GerarResponse(veiculoFIPE));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }
    }
}
