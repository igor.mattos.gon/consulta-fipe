﻿using AutoMapper;
using ConsultaFIPE.Apresentacao.WebApi.Filters;
using ConsultaFIPE.Apresentacao.WebApi.Model.Exception;
using ConsultaFIPE.Apresentacao.WebApi.Model.Response;
using ConsultaFIPE.Apresentacao.WebApi.Model;
using ConsultaFIPE.Dominio.Entidade.Model;
using ConsultaFIPE.Dominio.Negocio.Contract.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ConsultaFIPE.Apresentacao.WebApi.Controllers
{
    [Route("api/cliente")]
    [ApiController]
    [ValidateModel]
    public class ClienteController : ControllerBase
    {
        private readonly IDomainCliente _domainCliente;
        private readonly IMapper _mapper;

        public ClienteController(IDomainCliente domainCliente, IMapper mapper)
        {
            _domainCliente = domainCliente;
            _mapper = mapper;
        }

        /// <summary>
        /// API de cadastro
        /// </summary>
        /// <param name="cliente">Objeto Cliente</param>
        /// <returns>Cliente cadastrado</returns>
        [HttpPost()]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Post(ClienteModelCadastro cliente)
        {
            try
            {
                Cliente clienteCadastro = _mapper.Map<ClienteModelCadastro, Cliente>(cliente);
                await _domainCliente.Cadastrar(clienteCadastro);

                return Ok(ResponseFactory<Cliente>.GerarResponse(clienteCadastro));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

        /// <summary>
        /// API de atualzação
        /// </summary>
        /// <param name="cliente">Objeto Cliente</param>
        /// <returns></returns>
        [HttpPut()]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Put(ClienteModelAlteracao cliente)
        {
            try
            {
                Cliente clienteAlteracao = _mapper.Map<ClienteModelAlteracao, Cliente>(cliente);
                clienteAlteracao.UUID = Guid.Parse(cliente.UUID);

                await _domainCliente.Atualizar(clienteAlteracao);

                return Ok(ResponseFactory<string>.GerarResponse("Cliente atualizado com sucesso."));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

        /// <summary>
        /// Metodo de listagem
        /// </summary>                
        /// <param name="offset">Total de itens a serem pulados</param>
        /// <param name="limit">Total de itens por vez</param>
        /// <returns>Lista de Clientes</returns>
        [HttpGet("{offset}/{limit}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Get(int offset, int limit)
        {
            try
            {
                IEnumerable<Cliente> clientes = await _domainCliente.ListarTodos()
                                                                    .Skip(offset)
                                                                    .Take(limit)
                                                                    .OrderBy(f => f.Nome).ToListAsync();

                int totalClientes = _domainCliente.ListarTodos().Count();

                IEnumerable<ClienteModelListagem> clientesListagem = _mapper.Map<IEnumerable<Cliente>, IEnumerable<ClienteModelListagem>>(clientes);

                if (clientesListagem.Count() == 0)
                {
                    return NotFound(ResponseFactory<string>.GerarResponse("Nenhum cliente foi encontrado."));
                }

                return Ok(ResponseFactory<IEnumerable<ClienteModelListagem>>.GerarResponse(clientesListagem, totalClientes));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

        /// <summary>
        /// Metodo de listagem de clientes ativos
        /// </summary>                        
        /// <returns>Lista de Clientes</returns>
        [HttpGet("ativos")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetAtivos()
        {
            try
            {
                IEnumerable<Cliente> clientes = await _domainCliente.ListarporExpressao(f => f.Ativo)                                                                    
                                                                    .OrderBy(f => f.Nome).ToListAsync();                

                IEnumerable<ClienteModelListagem> clientesListagem = _mapper.Map<IEnumerable<Cliente>, IEnumerable<ClienteModelListagem>>(clientes);

                if (clientesListagem.Count() == 0)
                {
                    return NotFound(ResponseFactory<string>.GerarResponse("Nenhum cliente foi encontrado."));
                }

                return Ok(ResponseFactory<IEnumerable<ClienteModelListagem>>.GerarResponse(clientesListagem));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

        /// <summary>
        /// Metodo de listagem por ID
        /// </summary>                
        /// <param name="Id">UUID do Cliente</param>        
        /// <returns>Objeto Cliente</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetByUUID(string id)
        {
            try
            {
                Guid uuid = Guid.Parse(id);

                Cliente cliente = await _domainCliente.ListarporExpressao(f => f.UUID == uuid).FirstOrDefaultAsync();
                ClienteModelListagem clienteIndividual = _mapper.Map<Cliente, ClienteModelListagem>(cliente);

                if (clienteIndividual == null)
                {
                    return NotFound(ResponseFactory<string>.GerarResponse("Nenhum cliente foi encontrado."));
                }

                return Ok(ResponseFactory<ClienteModelListagem>.GerarResponse(clienteIndividual));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

    }
}
