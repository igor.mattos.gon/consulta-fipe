﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace ConsultaFIPE.Apresentacao.WebApi.Model
{
    [DataContract]
    public class ClienteModelCadastro
    {
        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        [DataMember(Name = "nome")]
        public string Nome { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(20)]
        [DataMember(Name = "alias")]
        public string Alias { get; set; }

        [DataMember(Name = "tempoVigenciaCacheVeiculo")]
        public int TempoVigenciaCacheVeiculo { get; set; }
    }

    [DataContract]
    public class ClienteModelAlteracao
    {
        [DataMember(Name = "uuid")]
        public string UUID { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        [DataMember(Name = "nome")]
        public string Nome { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        [DataMember(Name = "login")]
        public string Login { get; set; }

        [DataMember(Name = "tempoVigenciaCacheVeiculo")]
        public int TempoVigenciaCacheVeiculo { get; set; }

        [DataMember(Name = "ativo")]
        public bool Ativo { get; set; }
    }

    [DataContract]
    public class ClienteModelListagem
    {
        [DataMember(Name = "uuid")]
        public string UUID { get; set; }

        [DataMember(Name = "nome")]
        public string Nome { get; set; }

        [DataMember(Name = "alias")]
        public string Alias { get; set; }

        [DataMember(Name = "tempoVigenciaCacheVeiculo")]
        public int TempoVigenciaCacheVeiculo { get; set; }

        [DataMember(Name = "ativo")]
        public bool Ativo { get; set; }
    }

    [DataContract]
    public class ClienteModelCicloUsoListagem
    {
        [DataMember(Name = "uuid")]
        public Guid UUID { get; set; }

        [DataMember(Name = "nome")]
        public string Nome { get; set; }
    }
}
