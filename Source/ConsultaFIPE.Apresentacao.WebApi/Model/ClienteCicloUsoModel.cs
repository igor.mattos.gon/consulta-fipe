﻿using ConsultaFIPE.Dominio.Entidade.Enum;
using ConsultaFIPE.Dominio.Entidade.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ConsultaFIPE.Apresentacao.WebApi.Model
{
    [DataContract]
    public class ClienteCicloUsoModelCadastro
    {
        [DataMember(Name = "dataInicio")]
        public DateTime DataInicio { get; set; }

        [DataMember(Name = "qtdAcessoDisponivel")]
        [Range(1, int.MaxValue)]
        public int QtdAcessoDisponivel { get; set; }

        [DataMember(Name = "tempoVigencia")]
        [Range(1, int.MaxValue)]
        public int TempoVigencia { get; set; }

        [DataMember(Name = "renovacaoAuto")]
        public bool RenovacaoAuto { get; set; }

        [Required]
        [DataMember(Name = "UUIDCliente")]
        public string UUIDCliente { get; set; }
    }

    [DataContract]
    public class ClienteCicloUsoModelCancelamento
    {
        [Required]
        [DataMember(Name = "login")]
        public string Login { get; set; }

        [Required]
        [DataMember(Name = "idClienteClicloUso")]
        public int IdClienteClicloUso { get; set; }
    }

    [DataContract]
    public class ClienteCicloUsoListagem
    {
        [Required]
        [DataMember(Name = "idClienteClicloUso")]
        public int IdClienteClicloUso { get; set; }

        [DataMember(Name = "dataInicio")]
        public DateTime DataInicio { get; set; }

        [DataMember(Name = "dataFim")]
        public DateTime DataFim { get; set; }

        [DataMember(Name = "qtdAcessoDisponivel")]
        public int QtdAcessoDisponivel { get; set; }

        [DataMember(Name = "qtdAcessoTotal")]
        public int QtdAcessoTotal { get; set; }

        [DataMember(Name = "tempoVigencia")]
        public int TempoVigencia { get; set; }

        [DataMember(Name = "renovacaoAuto")]
        public bool RenovacaoAuto { get; set; }

        [DataMember(Name = "status")]
        public int Status { get; set; }

        [DataMember(Name = "observacao")]
        public string Observacao { get; set; }

        [DataMember(Name = "cliente")]
        public virtual ClienteModelCicloUsoListagem Cliente { get; set; }

        #region Propriedades Lógicas

        [DataMember(Name = "nomeStatus")]
        public string NomeStatus
        {
            get
            {
                if (Enum.IsDefined(typeof(StatusEnum.ClienteCicloUso), Status))
                {
                    return ((StatusEnum.ClienteCicloUso)Status).ToString();                    
                }

                else
                {
                    return "N/D";
                }
            }
        }

        #endregion
    }
}
