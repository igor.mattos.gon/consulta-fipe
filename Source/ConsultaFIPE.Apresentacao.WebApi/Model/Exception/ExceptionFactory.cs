﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace ConsultaFIPE.Apresentacao.WebApi.Model.Exception
{
    /// <summary>
    /// Classe responsável pelo tratamento de exceções
    /// </summary>    
    public static class ExceptionFactory
    {
        /// <summary>
        /// Método responsável por gerar o tratamento da exceção em um objeto
        /// </summary>
        /// <param name="ex">Objeto Exception</param>
        /// <returns>Exceção tratada</returns>
        public static ExceptionModel GerarExcecao(System.Exception ex)
        {
            ExceptionModel exceptionModel = null;

            if (ex != null)
            {
                exceptionModel = new ExceptionModel()
                {
                    Id = ex.HResult,
                    HelperLink = ex.HelpLink,
                    Message = ex.InnerException != null && ex.InnerException.InnerException != null ? ex.InnerException.InnerException.Message :
                          (ex.InnerException != null ? ex.InnerException.Message : ex.Message),
                    Source = ex.Source
                };
            }

            return exceptionModel;
        }        
    }

    [DataContract]
    public class ExceptionModel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "source")]
        public string Source { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "helperLink")]
        public string HelperLink { get; set; }
    }      
}
