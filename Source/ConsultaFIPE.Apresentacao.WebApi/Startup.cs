using Sigtrans.Uranus.Net.UranusCore.Infraestrutura.RepositorioEF.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System.Text;
using System.Collections;
using Microsoft.AspNetCore.Routing;
using ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.Model;
using ConsultaFIPE.Dominio.Negocio.Contract.Services;
using ConsultaFIPE.Dominio.Negocio.Service;
using ConsultaFIPE.Dominio.Negocio.Contract.Repository;
using ConsultaFIPE.Infraestrutura.RepositorioEF.Repository.Service;
using ConsultaFIPE.Dominio.ServicosExternos.FIPE;
using ConsultaFIPE.Infraestrutura.ServicosExternos.Renavam.FIPE;
using ConsultaFIPE.Infraestrutura.Utilidades.Cliente;
using ConsultaFIPE.Apresentacao.WebApi.Filters;
using ConsultaFIPE.Apresentacao.WebApi.Model.Exception;
using ConsultaFIPE.Apresentacao.WebApi.Model;
using ConsultaFIPE.Dominio.Entidade.Model;

namespace ConsultaFIPE.Apresentacao.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var envApp = Configuration.GetSection("ASPNETCORE_ENVIRONMENT").Value;
            var versionDotnet = Configuration.GetSection("DOTNET_VERSION").Value;
            var versionAspnet = Configuration.GetSection("ASPNET_VERSION").Value;
            var versionApp = Configuration.GetSection("Version").Value;
            var stringConexao = Configuration.GetSection("DB").Value;

            services.AddOptions();            

            //Adicionar classe de par�metros AWS
            IConfigurationSection awsSettingsSection = Configuration.GetSection("FIPEParameters");
            services.Configure<FIPEParameters>(awsSettingsSection);

            //Adicionar formata��o de data
            services.AddControllers().AddNewtonsoftJson
                (options =>
                {
                    options.SerializerSettings.DateFormatString = "dd/MM/yyyy hh:mm:ss";
                });

            //Configurar a string de conex�o
            services.AddDbContext<DataContext>(options =>
                                               options
                                               .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddDebug()))
                                               .UseLazyLoadingProxies()
                                               .UseSqlServer(stringConexao));

            //Inje��o de Depend�ncia do dom�nio
            services.AddScoped(typeof(IDomainCliente), typeof(DomainCliente));
            services.AddScoped(typeof(IDomainClienteCicloUso), typeof(DomainClienteCicloUso));
            services.AddScoped(typeof(IDomainHistoricoAtualizacaoCliente), typeof(DomainHistoricoAtualizacaoCliente));
            services.AddScoped(typeof(IDomainLog), typeof(DomainLog));
            services.AddScoped(typeof(IDomainVeiculo), typeof(DomainVeiculo));

            //Inje��o de Depend�ncia do reposit�rio Usu�rio            
            services.AddScoped(typeof(IRepositoryCliente), typeof(RepositoryCliente));
            services.AddScoped(typeof(IRepositoryClienteCicloUso), typeof(RepositoryClienteCicloUso));
            services.AddScoped(typeof(IRepositoryHistoricoAtualizacaoCliente), typeof(RepositoryHistoricoAtualizacaoCliente));
            services.AddScoped(typeof(IRepositoryLog), typeof(RepositoryLog));
            services.AddScoped(typeof(IRepositoryVeiculo), typeof(RepositoryVeiculo));

            //Inje��o de Depend�ncia de servi�os externos            
            services.AddScoped(typeof(IDomainIntegrationRenavamFIPE), typeof(DomainIntegrationRenavamFIPE));
            services.AddScoped(typeof(IIntegrationRenavamFIPE), typeof(IntegrationRenavamFIPE));

            //Inje��o de Depend�ncia de utilidades
            services.AddScoped(typeof(IWorkflowClienteValidation), typeof(WorkflowClienteValidation));

            //Auto Mapper
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                //Cadastro e Altera��o
                cfg.CreateMap<ClienteModelCadastro, Cliente>();
                cfg.CreateMap<ClienteModelAlteracao, Cliente>();
                cfg.CreateMap<ClienteCicloUsoModelCadastro, ClienteCicloUso>();

                //Listagem
                cfg.CreateMap<Cliente, ClienteModelListagem>();
                cfg.CreateMap<Cliente, ClienteModelCicloUsoListagem>();
                cfg.CreateMap<ClienteCicloUso, ClienteCicloUsoListagem>();
            });

            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            //Adicionar o Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { 
                    Title = $"Sigtrans.ConsultaVeiculos.WebApi ({envApp})", 
                    Description = $"Api de Consulta de veiculos - DotNet Version: {versionDotnet} - AspNet Version: {versionAspnet}",
                    Version = versionApp
                });
                c.OperationFilter<CustomParameterFilter>();
            });            

            services.AddHealthChecks();

            //Adi��o da Rota           
            services.AddRouting();

            //Adicionar o filtro de valida��o do modelo
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(ValidateModelAttribute));
            })
            .ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressMapClientErrors = true;
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Consulta Veiculos"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(x => x
               .AllowAnyMethod()
               .AllowAnyHeader()
               .SetIsOriginAllowed(origin => true)
               .AllowCredentials());

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseCors(c => c.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            //Configurar o handler para gera��o da exce��o que n�o for status code 400
            app.UseExceptionHandler(c => c.Run(async context =>
            {
                IExceptionHandlerPathFeature exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                Exception exception = exceptionHandlerPathFeature.Error;

                await context.Response.WriteAsJsonAsync(ExceptionFactory.GerarExcecao(exception));
            }));

            //endpoint health check
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");
            });

            //endpoint env vars
            StringBuilder stringBuilderEnv = GetEnvironmentVarsStringBuilder();
            app.UseEndpoints(endpoints => endpoints.MapGet("/env", async context =>
            {
                await context.Response.WriteAsync(stringBuilderEnv.ToString());
            }));
        }

        private static StringBuilder GetEnvironmentVarsStringBuilder()
        {
            StringBuilder stringBuilderEnv = new StringBuilder();
            foreach (DictionaryEntry dicEnv in Environment.GetEnvironmentVariables())
            {
                string name = (string)dicEnv.Key;
                string value = (string)dicEnv.Value;
                ; ; Console.WriteLine("{0}={1}", name, value);
                stringBuilderEnv.Append(name + "=" + value + " \n");
            }

            return stringBuilderEnv;
        }
    }
}
