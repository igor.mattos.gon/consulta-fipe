﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Entidade.Model
{
    public class Cliente
    {
        public Cliente()
        {
            DataCadastro = DateTime.Now;
            Ativo = true;
            UUID = Guid.NewGuid();

            CiclosUso = new List<ClienteCicloUso>();
        }

        public Guid UUID { get; set; }

        public int IdCliente { get; set; }

        public string Nome { get; set; }

        public string Alias { get; set; }

        public int TempoVigenciaCacheVeiculo { get; set; }

        public bool Ativo { get; set; }

        public DateTime DataCadastro { get; set; }

        public virtual ICollection<ClienteCicloUso> CiclosUso { get; set; }

        #region Propriedades Transientes

        [NotMapped]
        public string Login { get; set; }

        #endregion
    }
}
