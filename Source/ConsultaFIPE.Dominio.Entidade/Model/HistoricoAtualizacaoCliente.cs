﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Entidade.Model
{
    public class HistoricoAtualizacaoCliente
    {
        public HistoricoAtualizacaoCliente()
        {
            DataAtualizacao = DateTime.Now;
        }

        public int IdHistoricoAtualizacaoCliente { get; set; }

        public string Login { get; set; }

        public DateTime DataAtualizacao { get; set; }

        public string NomeAntigo { get; set; }

        public string NomeNovo { get; set; }

        public int TempoVigenciaAntigo { get; set; }

        public int TempoVigenciaNovo { get; set; }

        public bool AtivoAntigo { get; set; }

        public bool AtivoNovo { get; set; }

        public int IdCliente { get; set; }
        public virtual Cliente Cliente { get; set; }
    }
}
