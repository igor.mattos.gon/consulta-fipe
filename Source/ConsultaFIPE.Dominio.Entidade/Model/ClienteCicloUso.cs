﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Entidade.Model
{
    public class ClienteCicloUso
    {
        public int IdClienteClicloUso { get; set; }

        public DateTime DataInicio { get; set; }

        public DateTime DataFim { get; set; }

        public int QtdAcessoDisponivel { get; set; }

        public int QtdAcessoTotal { get; set; }

        public int TempoVigencia { get; set; }

        public bool RenovacaoAuto { get; set; }

        public int Status { get; set; }

        public string Observacao { get; set; }

        public int IdCliente { get; set; }
        public virtual Cliente Cliente { get; set; }
    }
}
