﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Entidade.Model
{
    public class Log
    {
        public Log()
        {
            Data = DateTime.Now;
        }

        public int IdLog { get; set; }

        public string Usuario { get; set; }

        public string Identificador { get; set; }

        public DateTime Data { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public int Tipo { get; set; }

        public string Valor { get; set; }        

        public int IdCliente { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}
