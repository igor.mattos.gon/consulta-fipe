﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Entidade.Model
{
    public class Veiculo
    {
        public int IdVeiculo { get; set; }

        public string UF { get; set; }

        public string Ano { get; set; }

        public string CMT { get; set; }

        public string Cor { get; set; }

        public string PBT { get; set; }

        public string Placa { get; set; }

        public string Chassi { get; set; }

        public string Motor { get; set; }

        public string Potencia { get; set; }

        public string Municipio { get; set; }        

        public string Cilindrada { get; set; }

        public string Combustivel { get; set; }

        public string Procedencia { get; set; }        

        public string MarcaModelo { get; set; }       

        public string Tipo { get; set; }

        public string CapacidadeCarga { get; set; }

        public string QtdEixo { get; set; }

        public string QtdPassageiro { get; set; }

        public DateTime? DataUltimaConsulta { get; set; }
    }
}
