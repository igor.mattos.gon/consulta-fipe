﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaFIPE.Dominio.Entidade.Enum
{
    public class StatusEnum
    {       
        public enum ClienteCicloUso
        {
            Ativo = 1,            
            Expirado = 2,
            Cancelado = 3
        }
    }
}
