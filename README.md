# Sigtrans - Consulta de Veiculos

Projeto criado para implementação default de consulta de veiculos com a FIPE.
O projeto utiliza a consulta da FIPE API por placa como consulta padrão.

## Fipe API

Link de documentação da consulta Fipe API por placa.

https://fipeapi.com.br/documentacao-placa.php

# Docker

Procedimento de deploy com docker.

## Buid da imagem

Execução para realizar o build da imagem.

```bash
$ docker build -t sigtrans/consulta-veiculo:v1.0.0 .
```

## Deploy applicação

Execute o comando para deploy da aplicação.
Utilize as variaveis de ambiente registradas para implantação.

Variaveis:

```bash
Version={versao da aplicacao correspondente a ser implantada}
```

```bash
ASPNETCORE_ENVIRONMENT={ambiente a ser implantado: Test, Development, Homolog, Production, etc...}
```

```bash
DB={string de conexao com banco de dados}
```

```bash
FIPEParameters.URL={url da api fipe consulta de placa}
```
```bash
FIPEParameters.Chave={chave de acesso (apikey) da api fipe consulta de placa}
```

Passar os parametros corretamente para deploy. Caso nao esteja correto a aplicação não ira funcionar corretamente.

obs: Verificar conforme o terminal a ser executado se as variaveis de ambiente necessitam de aspas na execução.

Para execucao em terminal que nao pede aspas "'"
```bash
$ docker run --name api_consveiculo -d -p 8090:80 -e Version=1.0.0 -e ASPNETCORE_ENVIRONMENT={ENV} -e DB={STRING_CONEXAO} -e FIPEParameters.URL={URL_API_FIPE} -e FIPEParameters.Chave={KEY_API_FIPE} sigtrans/consulta-veiculo:v1.0.0
```


Para execucao em terminal que necessita de aspas (simples) "'"
```bash
$ docker run --name api_consveiculo -d -p 8090:80 -e 'Version=1.0.0' -e 'ASPNETCORE_ENVIRONMENT={ENV}' -e 'DB={STRING_CONEXAO}' -e 'FIPEParameters.URL={URL_API_FIPE}' -e 'FIPEParameters.Chave={KEY_API_FIPE}' sigtrans/consulta-veiculo:v1.0.0
```

Exemplo de execucao:
```bash
$ docker run --name api_consveiculo -d -p 8090:80 -e 'Version=1.0.0' -e 'ASPNETCORE_ENVIRONMENT=Local' -e 'DB.ConnectionStrings=Server=localhost;Database=banco;Uid=user;Pwd=senha;MultipleActiveResultSets=true;TrustServerCertificate=true' -e 'FIPEParameters.URL=https://placas.fipeapi.com.br/placas/' -e 'FIPEParameters.Chave=minha_chave' sigtrans/consulta-veiculo:v1.0.0
```

